using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class CustomerManager : MonoBehaviour
{
    private static CustomerManager _instance;

    #region SingletonForCustomer

    public static CustomerManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<CustomerManager>();

                if (_instance == null)
                {
                    GameObject singletonObject = new GameObject("CustomerManager"); // Change the name to "CustomerManager"
                    _instance = singletonObject.AddComponent<CustomerManager>();
                }
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #endregion

    public enum Menu
    {
        Menu1,
        Menu2,
        Menu3
    }
    
    [SerializeField] public GameObject[] CustomerPrefab;
    
    public Transform[] Points;
    
    [SerializeField] public Transform CustomerSpawnPoint;

    public Transform ExitPoint;

    public bool CanPoint1 = true;
    public bool CanPoint2 = true;
    public bool CanPoint3 = true;
    public bool CanPoint4 = true;
    public bool CanPoint5 = true;

    public int MaxCustomers = 5;
    public int CurrentCustomer = 0;
    
    public float SpawnRate = 1f;
    private float nextSpawnTime = 0f;

    private void Start()
    {
        IsCustomerSpawned = new bool[CustomerPrefab.Length];
        for (int i = 0; i < IsCustomerSpawned.Length; i++)
        {
            IsCustomerSpawned[i] = false;
        }
    }

    private void Update()
    {
        if (Time.time >= nextSpawnTime)
        {
            StartCoroutine(SpawnDelay(0.5f));
            nextSpawnTime = Time.time + 1f / SpawnRate;
        }
    }

    public void SpawnCustomer()
    {
        if (CurrentCustomer < MaxCustomers)
        {
            int randomIndex = Random.Range(0, CustomerPrefab.Length);
            if (!IsCustomerSpawned[randomIndex])
            {
                Instantiate(CustomerPrefab[randomIndex], 
                    CustomerSpawnPoint.position, Quaternion.identity);
                IsCustomerSpawned[randomIndex] = true;
                CurrentCustomer++;
            }
        }
    }

    IEnumerator SpawnDelay(float time)
    {
        yield return new WaitForSeconds(time);
        SpawnCustomer();
    }
    
    public Menu GetRandomMenu()
    {
        Menu[] menus = (Menu[])System.Enum.GetValues(typeof(Menu));
        
        int randomIndex = Random.Range(0, menus.Length);
        
        return menus[randomIndex];
    }

    public bool[] IsCustomerSpawned;

    public void IncresedCustomer()
    {
        CurrentCustomer--;
    }
    
    public void OnCustomerDestroyed(int Index)
    {
        IsCustomerSpawned[Index] = false;
    }
}