using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.TextCore.Text;
using Random = UnityEngine.Random;

public class Customer : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    private int Way;
    
    public float MaxTime;
    public float CurrentTime;

    public TextMeshProUGUI timeText;
    private CheckMenu menuchecker;
    private GameObject objChecker;
    public enum customerNum
    {
        Customer1,
        Customer2,
        Customer3,
        Customer4,
        Customer5
    }
    
    public customerNum _customerNum;
    
    private Rigidbody2D rb;

    private bool Exit = false;
    
    private bool[] customerCheck = new bool[5];

    private bool isMoving = true;

    private bool forceStop = false;
    
    private void Start()
    {
        CustomerManager.Menu randomMenu = CustomerManager.Instance.GetRandomMenu();
        
        switch (_customerNum)
        {
            case customerNum.Customer1: objChecker = GameObject.Find("MenuChecker (1)");
                break;
            case customerNum.Customer2: objChecker = GameObject.Find("MenuChecker (2)");
                break;
            case customerNum.Customer3: objChecker = GameObject.Find("MenuChecker (3)");
                break;
            case customerNum.Customer4: objChecker = GameObject.Find("MenuChecker (4)");
                break;
            case customerNum.Customer5: objChecker = GameObject.Find("MenuChecker (5)");
                break;
        }
        menuchecker = objChecker.GetComponent<CheckMenu>();
        
        //Debug.Log("The Order is : " + menuchecker._currentMenu);
        
        MaxTime = Random.Range(10, 60);
        
        CurrentTime = MaxTime;
        
        rb = GetComponent<Rigidbody2D>();
        
        customerCheck[0] = true;
        customerCheck[1] = true;
        customerCheck[2] = true;
        customerCheck[3] = true;
        customerCheck[4] = true;
    }

    private void Update()
    {
        if (_customerNum == customerNum.Customer1 && CustomerManager.Instance.CanPoint1 == true)
        {
            transform.position = Vector2.MoveTowards(transform.position,
                CustomerManager.Instance.Points[0].transform.position,
                moveSpeed * Time.deltaTime);
        }
        if (_customerNum == customerNum.Customer2 && CustomerManager.Instance.CanPoint2 == true)
        {
            transform.position = Vector2.MoveTowards(transform.position,
                CustomerManager.Instance.Points[1].transform.position,
                moveSpeed * Time.deltaTime);
        }
        if (_customerNum == customerNum.Customer3 && CustomerManager.Instance.CanPoint3 == true)
        {
            transform.position = Vector2.MoveTowards(transform.position,
                CustomerManager.Instance.Points[2].transform.position,
                moveSpeed * Time.deltaTime);
        }
        if (_customerNum == customerNum.Customer4 && CustomerManager.Instance.CanPoint4 == true)
        {
            transform.position = Vector2.MoveTowards(transform.position,
                CustomerManager.Instance.Points[3].transform.position,
                moveSpeed * Time.deltaTime);
        }
        if (_customerNum == customerNum.Customer5 && CustomerManager.Instance.CanPoint5 == true)
        {
            transform.position = Vector2.MoveTowards(transform.position,
                CustomerManager.Instance.Points[4].transform.position,
                moveSpeed * Time.deltaTime);
        }
        
        if (rb.velocity.magnitude <= 0.1f && forceStop == false)
        {
            Debug.LogWarning("Heee");
            switch (_customerNum)
            {
                case customerNum.Customer1: 
                    ShopControl.Instance.CreateRecipe(GlobalManager.Instance.Table1CurrentMenu);
                    break;
                case customerNum.Customer2: 
                    ShopControl.Instance.CreateRecipe(GlobalManager.Instance.Table2CurrentMenu); 
                    break;
                case customerNum.Customer3: 
                    ShopControl.Instance.CreateRecipe(GlobalManager.Instance.Table3CurrentMenu);
                    break;
                case customerNum.Customer4: 
                    ShopControl.Instance.CreateRecipe(GlobalManager.Instance.Table4CurrentMenu);
                    break;
                case customerNum.Customer5: 
                    ShopControl.Instance.CreateRecipe(GlobalManager.Instance.Table5CurrentMenu);
                    break;
            }
            
            forceStop = true;
            if (isMoving == true)
            {
                isMoving = false;
                Debug.Log("stopped moving");
            }
        }

        if (forceStop == true)
        {
            CountingTime();
            
        }

        if (menuchecker._currentMenu == 0)
        {
            Debug.Log("กูไปละ");
            Exit = true;

            StartCoroutine(Wait());
        }

        if (CurrentTime <= 0)
        {
            Exit = true;
            StartCoroutine(Wait());
        }

        if (Exit == true)
        {
            switch (_customerNum)
            {
                case customerNum.Customer1: 
                    ShopControl.Instance.ResetMenu(1); 
                    break;
                case customerNum.Customer2: 
                    ShopControl.Instance.ResetMenu(2); 
                    break;
                case customerNum.Customer3: 
                    ShopControl.Instance.ResetMenu(3); 
                    break;
                case customerNum.Customer4: 
                    ShopControl.Instance.ResetMenu(4); 
                    break;
                case customerNum.Customer5: 
                    ShopControl.Instance.ResetMenu(5); 
                    break;
            }
            
            GoExit();
            timeText.text = " ";
        }
        
    }

    private void CountingTime()
    {
        switch (_customerNum)
        {
            case customerNum.Customer1: ShopControl.Instance.SetMenu1(); break;
            case customerNum.Customer2: ShopControl.Instance.SetMenu2(); break;
            case customerNum.Customer3: ShopControl.Instance.SetMenu3(); break;
            case customerNum.Customer4: ShopControl.Instance.SetMenu4(); break;
            case customerNum.Customer5: ShopControl.Instance.SetMenu5(); break;
        }
        
        CurrentTime -= Time.deltaTime;
        timeText.text = CurrentTime.ToString("f0");
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);

        switch (_customerNum)
        {
            case customerNum.Customer1:
                CustomerManager.Instance.OnCustomerDestroyed(0);
                CustomerManager.Instance.IncresedCustomer();
                break;
            case customerNum.Customer2:
                CustomerManager.Instance.OnCustomerDestroyed(1);
                CustomerManager.Instance.IncresedCustomer();
                break;
            case customerNum.Customer3:
                CustomerManager.Instance.OnCustomerDestroyed(2);
                CustomerManager.Instance.IncresedCustomer();
                break;
            case customerNum.Customer4:
                CustomerManager.Instance.OnCustomerDestroyed(3);
                CustomerManager.Instance.IncresedCustomer();
                break;
            case customerNum.Customer5:
                CustomerManager.Instance.OnCustomerDestroyed(4);
                CustomerManager.Instance.IncresedCustomer();
                break;
        }
    }

    private void GoExit()
    {
        transform.position = Vector2.MoveTowards(transform.position,
            CustomerManager.Instance.ExitPoint.transform.position,
            (moveSpeed * 3f) * Time.deltaTime);
    }
}
