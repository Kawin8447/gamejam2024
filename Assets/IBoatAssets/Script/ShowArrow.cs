using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowArrow : MonoBehaviour
{
    public enum ArrowType
    {
        Ingre,
        Proc,
        Cook,
        menu
    }

    public ArrowType _Arrow;
    public GameObject _arrowObj;


    private void Update()
    {
        if (PlayerMove.Instance.isGetingredient == false && _Arrow == ArrowType.Ingre && PlayerMove.Instance.isGetProcess == false && PlayerMove.Instance.isGetCooking == false)
        {
            _arrowObj.SetActive(true);
        }
        else if (PlayerMove.Instance.isGetingredient == true && _Arrow == ArrowType.Ingre)
        {
            _arrowObj.SetActive(false);
        }
        if (PlayerMove.Instance.isGetProcess == false && _Arrow == ArrowType.Proc && PlayerMove.Instance.isGetingredient ==true)
        {
            _arrowObj.SetActive(true);
        }
        else if (PlayerMove.Instance.isGetProcess == true && _Arrow == ArrowType.Proc)
        {
            _arrowObj.SetActive(false);
        }
        if (PlayerMove.Instance.isGetCooking == false && _Arrow == ArrowType.Cook && PlayerMove.Instance.isGetProcess == true)
        {
            _arrowObj.SetActive(true);
        }
        else if (PlayerMove.Instance.isGetCooking == true && _Arrow == ArrowType.Cook)
        {
            _arrowObj.SetActive(false);
        }
        if (PlayerMove.Instance.isGetCooking == true && _Arrow == ArrowType.menu)
        {
            _arrowObj.SetActive(true);
        }
        else if (PlayerMove.Instance.isGetCooking == false && _Arrow == ArrowType.menu)
        {
            _arrowObj.SetActive(false);
        }
    }
}
