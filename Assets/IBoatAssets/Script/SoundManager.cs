using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource audioSource;

    // Reference to the sound effect AudioClip
    public AudioClip soundEffect;

    // Method to play the sound effect
    public void PlaySoundEffect()
    {
        // Check if AudioSource component and AudioClip are valid
        if (audioSource != null && soundEffect != null)
        {
            // Play the sound effect
            audioSource.PlayOneShot(soundEffect);
        }
    }
}
