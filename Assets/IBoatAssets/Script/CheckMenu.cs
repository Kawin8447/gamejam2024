using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CheckMenu : MonoBehaviour
{
    private PlayerMove Obj;
    public MenuObject[] menuNum;
    public int _currentMenu;
    public bool _isMenuActive;
    public bool _Wrong;
    private static CheckMenu _instance;
    public SoundManager soundManager;
    
    public static CheckMenu Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<CheckMenu>();

                if (_instance == null)
                {
                    GameObject singletonObject = new GameObject("CustomerManager"); // Change the name to "CustomerManager"
                    _instance = singletonObject.AddComponent<CheckMenu>();
                }
            }

            return _instance;
        }
    }
    [SerializeField] private int _orderIngredeintUse;
    [SerializeField] private int _orderProcessUse;
    [SerializeField] private int _orderCookingUse;

    
    private void Update()
    {
        if (_isMenuActive == false&&_currentMenu == 0)
        {
            _isMenuActive = true;
            _currentMenu = Random.Range(1,8);
        }

        switch (_currentMenu)
        {
            case 0: _isMenuActive = false;
                _Wrong = false;
                break;
            case 1: _orderIngredeintUse = menuNum[0].IngredientNumber;
                _orderProcessUse = menuNum[0].ProcessNumber;
                _orderCookingUse = menuNum[0].CookingNumber;
                
                break;
            case 2: _orderIngredeintUse = menuNum[1].IngredientNumber;
                _orderProcessUse = menuNum[1].ProcessNumber;
                _orderCookingUse = menuNum[1].CookingNumber;
                
                break;
            case 3: _orderIngredeintUse = menuNum[2].IngredientNumber;
                _orderProcessUse = menuNum[2].ProcessNumber;
                _orderCookingUse = menuNum[2].CookingNumber;
                
                break;
            case 4: _orderIngredeintUse = menuNum[3].IngredientNumber;
                _orderProcessUse = menuNum[3].ProcessNumber;
                _orderCookingUse = menuNum[3].CookingNumber;
               
                break;
            case 5: _orderIngredeintUse = menuNum[4].IngredientNumber;
                _orderProcessUse = menuNum[4].ProcessNumber;
                _orderCookingUse = menuNum[4].CookingNumber;
               
                break;
            case 6:_orderIngredeintUse = menuNum[5].IngredientNumber;
                _orderProcessUse = menuNum[5].ProcessNumber;
                _orderCookingUse = menuNum[5].CookingNumber;
                
                break;
            case 7:_orderIngredeintUse = menuNum[6].IngredientNumber;
                _orderProcessUse = menuNum[6].ProcessNumber;
                _orderCookingUse = menuNum[6].CookingNumber;
               
                break;
            case 8: _orderIngredeintUse = menuNum[7].IngredientNumber;
                _orderProcessUse = menuNum[7].ProcessNumber;
                _orderCookingUse = menuNum[7].CookingNumber;
               
                break;
        }
        
        if (gameObject.name == "MenuChecker (1)")
        {
            GlobalManager.Instance.Table1CurrentMenu = _currentMenu;
        }
        else if (gameObject.name == "MenuChecker (2)")
        {
            GlobalManager.Instance.Table2CurrentMenu = _currentMenu;
        }
        else if (gameObject.name == "MenuChecker (3)")
        {
            GlobalManager.Instance.Table3CurrentMenu = _currentMenu;
        }
        else if (gameObject.name == "MenuChecker (4)")
        {
            GlobalManager.Instance.Table4CurrentMenu = _currentMenu;
        }
        else if (gameObject.name == "MenuChecker (5)")
        {
            GlobalManager.Instance.Table5CurrentMenu = _currentMenu;
        }
        
        /*Debug.Log("Table 1 : " + GlobalManager.Instance.Table1CurrentMenu);
        Debug.Log("Table 2 : " + GlobalManager.Instance.Table2CurrentMenu);
        Debug.Log("Table 3 : " + GlobalManager.Instance.Table3CurrentMenu);
        Debug.Log("Table 4 : " + GlobalManager.Instance.Table4CurrentMenu);
        Debug.Log("Table 5 : " + GlobalManager.Instance.Table5CurrentMenu);*/
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            Obj = collider.gameObject.GetComponent<PlayerMove>();
            if (gameObject.name == "MenuChecker (1)" && Obj.selectTarget == 1)
            {
                if (Obj._playerIngredientNumber == _orderIngredeintUse &&
                    Obj._playerProcessNumber == _orderProcessUse &&
                    Obj._playerCookingNumber == _orderCookingUse &&
                    _isMenuActive == true)
                {
                    Debug.Log("Menu: "+menuNum[_currentMenu - 1].MenuName+" Complete");
                    Debug.Log("Add money "+menuNum[_currentMenu - 1].MenuValue);
                    ShopControl.Instance.moneyAmount += menuNum[_currentMenu - 1].MenuValue;
                    ShopControl.Instance.moneyAmount += menuNum[_currentMenu - 1].MenuValue;
                    Obj._playerIngredientNumber = 0;
                    Obj._playerProcessNumber = 0;
                    Obj._playerCookingNumber = 0;
                    Obj.isGetCooking = false;
                    ShopControl.Instance.ResetRecipe(_currentMenu);
                    _currentMenu = 0;
                }
                else
                {
                    Debug.Log("You fuck up");
                    _Wrong = true;
                    Obj._playerIngredientNumber = 0;
                    Obj._playerProcessNumber = 0;
                    Obj._playerCookingNumber = 0;
                    Obj.isGetCooking = false;
                    ShopControl.Instance.ResetRecipe(_currentMenu);
                    GlobalManager.Instance.currentHealth += 5;
                    soundManager.PlaySoundEffect();
                    _currentMenu = 0;
                }
            }
            if (gameObject.name == "MenuChecker (2)" && Obj.selectTarget == 2)
            {
                if (Obj._playerIngredientNumber == _orderIngredeintUse &&
                    Obj._playerProcessNumber == _orderProcessUse &&
                    Obj._playerCookingNumber == _orderCookingUse &&
                    _isMenuActive == true)
                {
                    Debug.Log("Menu: "+menuNum[_currentMenu - 1].MenuName+" Complete");
                    Debug.Log("Add money "+menuNum[_currentMenu - 1].MenuValue);
                    ShopControl.Instance.moneyAmount += menuNum[_currentMenu - 1].MenuValue;
                    ShopControl.Instance.moneyAmount += menuNum[_currentMenu - 1].MenuValue;
                    Obj._playerIngredientNumber = 0;
                    Obj._playerProcessNumber = 0;
                    Obj._playerCookingNumber = 0;
                    Obj.isGetCooking = false;
                    ShopControl.Instance.ResetRecipe(_currentMenu);
                    _currentMenu = 0;
                }
                else
                {
                    Debug.Log("You fuck up");
                    _Wrong = true;
                    Obj._playerIngredientNumber = 0;
                    Obj._playerProcessNumber = 0;
                    Obj._playerCookingNumber = 0;
                    Obj.isGetCooking = false;
                    ShopControl.Instance.ResetRecipe(_currentMenu);
                    GlobalManager.Instance.currentHealth += 5;
                    soundManager.PlaySoundEffect();
                    _currentMenu = 0;
                }
            }
            if (gameObject.name == "MenuChecker (3)" && Obj.selectTarget == 3)
            {
                if (Obj._playerIngredientNumber == _orderIngredeintUse &&
                    Obj._playerProcessNumber == _orderProcessUse &&
                    Obj._playerCookingNumber == _orderCookingUse &&
                    _isMenuActive == true)
                {
                    Debug.Log("Menu: "+menuNum[_currentMenu - 1].MenuName+" Complete");
                    Debug.Log("Add money "+menuNum[_currentMenu - 1].MenuValue);
                    ShopControl.Instance.moneyAmount += menuNum[_currentMenu - 1].MenuValue;
                    ShopControl.Instance.moneyAmount += menuNum[_currentMenu - 1].MenuValue;
                    Obj._playerIngredientNumber = 0;
                    Obj._playerProcessNumber = 0;
                    Obj._playerCookingNumber = 0;
                    Obj.isGetCooking = false;
                    ShopControl.Instance.ResetRecipe(_currentMenu);
                    _currentMenu = 0;
                }
                else
                {
                    Debug.Log("You fuck up");
                    _Wrong = true;
                    Obj._playerIngredientNumber = 0;
                    Obj._playerProcessNumber = 0;
                    Obj._playerCookingNumber = 0;
                    Obj.isGetCooking = false;
                    soundManager.PlaySoundEffect();
                    ShopControl.Instance.ResetRecipe(_currentMenu);
                    GlobalManager.Instance.currentHealth += 5;
                    _currentMenu = 0;
                }
            }
            if (gameObject.name == "MenuChecker (4)" && Obj.selectTarget == 4)
            {
                if (Obj._playerIngredientNumber == _orderIngredeintUse &&
                    Obj._playerProcessNumber == _orderProcessUse &&
                    Obj._playerCookingNumber == _orderCookingUse &&
                    _isMenuActive == true)
                {
                    Debug.Log("Menu: "+menuNum[_currentMenu - 1].MenuName+" Complete");
                    Debug.Log("Add money "+menuNum[_currentMenu - 1].MenuValue);
                    ShopControl.Instance.moneyAmount += menuNum[_currentMenu - 1].MenuValue;
                    ShopControl.Instance.moneyAmount += menuNum[_currentMenu - 1].MenuValue;
                    Obj._playerIngredientNumber = 0;
                    Obj._playerProcessNumber = 0;
                    Obj._playerCookingNumber = 0;
                    Obj.isGetCooking = false;
                    ShopControl.Instance.ResetRecipe(_currentMenu);
                    _currentMenu = 0;
                }
                else
                {
                    Debug.Log("You fuck up");
                    _Wrong = true;
                    Obj._playerIngredientNumber = 0;
                    Obj._playerProcessNumber = 0;
                    Obj._playerCookingNumber = 0;
                    Obj.isGetCooking = false;
                    ShopControl.Instance.ResetRecipe(_currentMenu);
                    GlobalManager.Instance.currentHealth += 5;
                    soundManager.PlaySoundEffect();
                    _currentMenu = 0;
                }
            }
            if (gameObject.name == "MenuChecker (5)" && Obj.selectTarget == 5)
            {
                if (Obj._playerIngredientNumber == _orderIngredeintUse &&
                    Obj._playerProcessNumber == _orderProcessUse &&
                    Obj._playerCookingNumber == _orderCookingUse &&
                    _isMenuActive == true)
                {
                    Debug.Log("Menu: "+menuNum[_currentMenu - 1].MenuName+" Complete");
                    Debug.Log("Add money "+menuNum[_currentMenu - 1].MenuValue);
                    ShopControl.Instance.moneyAmount += menuNum[_currentMenu - 1].MenuValue;
                    ShopControl.Instance.moneyAmount += menuNum[_currentMenu - 1].MenuValue;
                    Obj._playerIngredientNumber = 0;
                    Obj._playerProcessNumber = 0;
                    Obj._playerCookingNumber = 0;
                    Obj.isGetCooking = false;
                    ShopControl.Instance.ResetRecipe(_currentMenu);
                    _currentMenu = 0;
                }
                else
                {
                    Debug.Log("You fuck up");
                    _Wrong = true;
                    Obj._playerIngredientNumber = 0;
                    Obj._playerProcessNumber = 0;
                    Obj._playerCookingNumber = 0;
                    Obj.isGetCooking = false;
                    ShopControl.Instance.ResetRecipe(_currentMenu);
                    GlobalManager.Instance.currentHealth += 5;
                    soundManager.PlaySoundEffect();
                    _currentMenu = 0;
                }
            }
        }
        
    }
}
