using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GlobalManager : MonoBehaviour
{
    #region Singleton

    private static GlobalManager _instance;
    
    public static GlobalManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GlobalManager>();

                if (_instance == null)
                {
                    GameObject singletonObject = new GameObject("GlobalManager"); // Change the name to "GlobalManager"
                    _instance = singletonObject.AddComponent<GlobalManager>();
                }
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #endregion
    
    [Header("Price of each ingredient")]
    public int Ingredient1Price;
    public int Ingredient2Price;
    public int Ingredient3Price;
    
    [Header("Text Ui of each ingredient")]
    public TextMeshProUGUI[] Ingredienttext;
    
    [Header("Health")]
    public float startingHealth = 100f;
    public float decreaseRate = 5f;
    public float currentHealth;
    
    public int Table1CurrentMenu;
    public int Table2CurrentMenu;
    public int Table3CurrentMenu;
    public int Table4CurrentMenu;
    public int Table5CurrentMenu;

    public Texture2D[] Order = new Texture2D[7];
    public float EventCooldown = 20f;
    public bool isEventInactive;
    public int _eventNumber;
    public float _curEventCooldown;
    public GameObject[] _mazeNum;
    public GameObject _hideBox;
    public AudioSource audioSource;
    public AudioClip soundEffect;

    public TextMeshProUGUI Timetext;
    public GameObject uiToolkit;
    public GameObject losePanel;

    private bool canLose = true;
    
    private void Start()
    {
        currentHealth = startingHealth;
        _curEventCooldown = 18;
    }
    private void Update()
    {
        Ingredienttext[0].text = ShopControl.Instance.flourInt.ToString("f0");
        Ingredienttext[1].text = ShopControl.Instance.meatInt.ToString("f0");
        Ingredienttext[2].text = ShopControl.Instance.vegetableInt.ToString("f0");
        if (isEventInactive == false)
        {
            _curEventCooldown -= Time.deltaTime;
        }
        if (_curEventCooldown <= 0)
        {
            _eventNumber = Random.Range(1, 5);
            isEventInactive = true;
            _curEventCooldown += EventCooldown;
        }

        if (currentHealth > 100)
        {
            currentHealth = 100;
        }
        switch (_eventNumber)
        {
            case 1:
                if (isEventInactive == true)
                {
                    audioSource.PlayOneShot(soundEffect);
                    Prank_Speed();
                }
                isEventInactive = false;
                break;
            case 2:
                if (isEventInactive == true)
                {
                    audioSource.PlayOneShot(soundEffect);
                    Prank_Suffer();
                }
                isEventInactive = false;
                break;
            case 3:
                if (isEventInactive == true)
                {
                    audioSource.PlayOneShot(soundEffect);
                    Prank_maze();
                }
                isEventInactive = false;
                break;
            case 4:
                if (isEventInactive == true)
                {
                    audioSource.PlayOneShot(soundEffect);
                    Prank_Hideingredient();
                }
                isEventInactive = false;
                break;
        }
            if (Input.GetKeyDown(KeyCode.E))
        {
            ShopControl.Instance.moneyAmount += 50;
        }
        
        DecreaseHealthOverTime();
        
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Prank_Speed();
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Prank_Suffer();
        }
        
        minutes = Mathf.FloorToInt(elapsedTime / 60f);
        seconds = Mathf.FloorToInt(elapsedTime % 60f);
        elapsedTime += Time.deltaTime;
        
    }

    public int minutes;
    public int seconds;
    
    public float elapsedTime;
    private void DecreaseHealthOverTime()
    {
        currentHealth = Mathf.Max(0f, currentHealth - decreaseRate * Time.deltaTime);
        
        if (currentHealth <= 0f && canLose == true)
        {
            HandleZeroHealth();
        }
    }
    private void HandleZeroHealth()
    {
        canLose = false;
        Debug.Log("Health reached 0");
        
        Timetext.text = string.Format("Survive : {0:00}:{1:00}", minutes, seconds);
        
        losePanel.SetActive(true);
        uiToolkit.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public float playerSpeed = 3;
    public float Prank_SpeedTime = 5;
    private bool speedDecreased = false;
    public void Prank_Speed()
    {
        if (!speedDecreased)
        {
            DecreaseSpeed();
            Invoke("ResetSpeed", 5.0f);
        }
    }
    private void DecreaseSpeed()
    {
        playerSpeed = playerSpeed/2;
        speedDecreased = true;
    }

    private void ResetSpeed()
    {
        playerSpeed = 3;
        speedDecreased = false;
    }

    public int newIngredient;
    public void Prank_Suffer()
    {
        if (PlayerMove.Instance.isGetingredient == true)
        {
            newIngredient = UnityEngine.Random.Range(1, 4);
            
            if (newIngredient == PlayerMove.Instance._playerIngredientNumber)
            {
                newIngredient = UnityEngine.Random.Range(1, 4);
                PlayerMove.Instance._playerIngredientNumber = newIngredient;
            }
            else PlayerMove.Instance._playerIngredientNumber = newIngredient;
        }
        Debug.Log(" newIngredient : " + newIngredient);
    }

    public void Prank_maze()
    {
        int _ranMaze;
        if (isEventInactive == true)
        {
            _ranMaze = Random.Range(0, 3);
            _mazeNum[_ranMaze].SetActive(true);
            StartCoroutine(Wait());
        }
        IEnumerator Wait()
        {
            yield return new WaitForSeconds(8f);
            _mazeNum[_ranMaze].SetActive(false);
        }
    }

    public void Prank_Hideingredient()
    {
        if (isEventInactive == true)
        {
            _hideBox.SetActive(true);
            StartCoroutine(Wait());
        }
        IEnumerator Wait()
        {
            yield return new WaitForSeconds(8f);
            _hideBox.SetActive(false);
        }
    }
}
