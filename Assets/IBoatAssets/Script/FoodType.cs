using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Type
{
    Ingredient,
    Process,
    Cooking,
    Checker
};
public class FoodType : MonoBehaviour
{
    [Header("ProcessType")] 
    public Type _Type;

    public int number;
}
