using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using Button = UnityEngine.UIElements.Button;

public class ShopControl : MonoBehaviour
{
    #region Singleton

    private static ShopControl _instance;
    
    public static ShopControl Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ShopControl>();

                if (_instance == null)
                {
                    GameObject singletonObject = new GameObject("UiManager"); // Change the name to "UiManager"
                    _instance = singletonObject.AddComponent<ShopControl>();
                }
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #endregion
    
    private VisualElement _buyScreen;

    private Button _openShop;
    private Button _confirmBuy;
    private Button _cancelBuy;

    #region Shop variable
    
    private Label _flourLabel;
    private Label _meatLabel;
    private Label _vegetableLabel;

    private Button _increase1;
    private Button _increase2;
    private Button _increase3;
    private Button _decrease1;
    private Button _decrease2;
    private Button _decrease3;

    private TextField _textField1;
    private TextField _textField2;
    private TextField _textField3;

    private int _flourAmount;
    private int _meatAmount;
    private int _vegetableAmount;

    public int flourInt;
    public int meatInt;
    public int vegetableInt;

    public int flourCost = 10;
    public int meatCost = 10;
    public int vegetableCost = 10;
    
    #endregion"

    private Label _timeLabel;
    private Label _moneyLabel;

    private float _timeRecord;
    public int moneyAmount;

    #region Customer variable

    private VisualElement _customerOrder;
    private VisualElement _customerOrder2;
    private VisualElement _customerOrder3;
    private VisualElement _customerOrder4;
    private VisualElement _customerOrder5;

    #endregion

    #region Menu Recipe variable

    private VisualElement _menu;
    private VisualElement _menu2;
    private VisualElement _menu3;
    private VisualElement _menu4;
    private VisualElement _menu5;
    private VisualElement _menu6;
    private VisualElement _menu7;
    private VisualElement _menu8;

    #endregion

    private VisualElement HealthBar;

    private Label Cost;
    private int total = 0;

    private VisualElement[] Recipe = new VisualElement[8];
    
    // Start is called before the first frame update
    void Start()
    {
        var root = GetComponent<UIDocument>().rootVisualElement;

        #region Shop Init

        _buyScreen = root.Q<VisualElement>("BuyScreen");
        _openShop = root.Q<Button>("Buy");
        _cancelBuy = root.Q<Button>("Button_Cancel");
        _confirmBuy = root.Q<Button>("Button_Buy");

        _buyScreen.style.display = DisplayStyle.None;
        
        _openShop.RegisterCallback<ClickEvent>(OnOpenShopButtonClicked);
        _cancelBuy.RegisterCallback<ClickEvent>(OnCancelButtonClicked);
        _confirmBuy.RegisterCallback<ClickEvent>(OnConfirmButtonClicked);
        #endregion

        #region Buying ingrediants Init

        _flourLabel = root.Q<Label>("Flour_amount");
        _meatLabel = root.Q<Label>("Meat_amount");
        _vegetableLabel = root.Q<Label>("Vegetable_amount");

        _increase1 = root.Q<Button>("Button_Increase-1");
        _increase2 = root.Q<Button>("Button_Increase-2");
        _increase3 = root.Q<Button>("Button_Increase-3");
        
        _decrease1 = root.Q<Button>("Button_Decrease-1");
        _decrease2 = root.Q<Button>("Button_Decrease-2");
        _decrease3 = root.Q<Button>("Button_Decrease-3");

        _textField1 = root.Q<TextField>("MyTextField-1");
        _textField2 = root.Q<TextField>("MyTextField-2");
        _textField3 = root.Q<TextField>("MyTextField-3");

        //SetCurrentIngredients();
        
        _textField1.RegisterValueChangedCallback(OnValue1Changed);
        _textField2.RegisterValueChangedCallback(OnValue2Changed);
        _textField3.RegisterValueChangedCallback(OnValue3Changed);
        
        _increase1.RegisterCallback<ClickEvent>(OnIncrease1clicked);
        _increase2.RegisterCallback<ClickEvent>(OnIncrease2clicked);
        _increase3.RegisterCallback<ClickEvent>(OnIncrease3clicked);
        _decrease1.RegisterCallback<ClickEvent>(OnDecrease1clicked);
        _decrease2.RegisterCallback<ClickEvent>(OnDecrease2clicked);
        _decrease3.RegisterCallback<ClickEvent>(OnDecrease3clicked);

        #endregion

        _timeLabel = root.Q<Label>("Timer");
        _moneyLabel = root.Q<Label>("Money");

        #region CustomersOrder Init

        _customerOrder = root.Q<VisualElement>("Customer_Order");
        _customerOrder2 = root.Q<VisualElement>("Customer_Order2");
        _customerOrder3 = root.Q<VisualElement>("Customer_Order3");
        _customerOrder4 = root.Q<VisualElement>("Customer_Order4");
        _customerOrder5 = root.Q<VisualElement>("Customer_Order5");

        #endregion

        #region Menu Recipe Init

        _menu = root.Q<VisualElement>("Menu");
        _menu2 = root.Q<VisualElement>("Menu-2");
        _menu3 = root.Q<VisualElement>("Menu-3");
        _menu4 = root.Q<VisualElement>("Menu-4");
        _menu5 = root.Q<VisualElement>("Menu-5");
        _menu6 = root.Q<VisualElement>("Menu-6");
        _menu7 = root.Q<VisualElement>("Menu-7");
        _menu8 = root.Q<VisualElement>("Menu-8");

        #endregion

        HealthBar = root.Q<VisualElement>("HpCell");

        _customerOrder.style.backgroundImage = GlobalManager.Instance.Order[8];
        _customerOrder2.style.backgroundImage = GlobalManager.Instance.Order[8];
        _customerOrder3.style.backgroundImage = GlobalManager.Instance.Order[8];
        _customerOrder4.style.backgroundImage = GlobalManager.Instance.Order[8];
        _customerOrder5.style.backgroundImage = GlobalManager.Instance.Order[8];
        
        Cost = root.Q<Label>("paidCost");

        for (int i = 0; i < 8; i++)
        {
            Recipe[i] = root.Q<VisualElement>("Menu-" + (i + 1));
            Recipe[i].style.display = DisplayStyle.None;
        }
    }

    private void OnOpenShopButtonClicked(ClickEvent evt)
    {
        _buyScreen.style.display = DisplayStyle.Flex;
        _buyScreen.AddToClassList("BuyScreen-Up");

        _textField1.value = 0.ToString();
        _textField2.value = 0.ToString();
        _textField3.value = 0.ToString();
        

}

    private void OnCancelButtonClicked(ClickEvent evt)
    {
        _buyScreen.style.display = DisplayStyle.None;
        _buyScreen.RemoveFromClassList("BuyScreen-Up");
        _flourAmount = 0;
        _meatAmount = 0;
        _vegetableAmount = 0;
    }

    private void OnConfirmButtonClicked(ClickEvent evt)
    {
        _buyScreen.style.display = DisplayStyle.None;
        if (total <= moneyAmount)
        {
            moneyAmount -= total;
            _buyScreen.RemoveFromClassList("BuyScreen-Up");
            AddCurrentIngredients();
            SetCurrentIngredients();
        }
        else
        {
            _buyScreen.style.display = DisplayStyle.None;
            _buyScreen.RemoveFromClassList("BuyScreen-Up");
            _flourAmount = 0;
            _meatAmount = 0;
            _vegetableAmount = 0;
        }
    }

    #region In/Decrease function

    private void OnIncrease1clicked(ClickEvent evt)
    {
        string textFieldValue = _textField1.value;
        if (int.TryParse(textFieldValue, out _flourAmount))
        {
            _flourAmount++;
            string updatedText = _flourAmount.ToString();
            
            _textField1.value = updatedText;
        }
        else
        {
            Debug.LogWarning("Invalid integer value in text field.");
        }
    }
    private void OnIncrease2clicked(ClickEvent evt)
    {
        string textFieldValue = _textField2.value;
        if (int.TryParse(textFieldValue, out _meatAmount))
        {
            _meatAmount++;
            string updatedText = _meatAmount.ToString();
            
            _textField2.value = updatedText;
        }
        else
        {
            Debug.LogWarning("Invalid integer value in text field.");
        }
    }
    private void OnIncrease3clicked(ClickEvent evt)
    {
        string textFieldValue = _textField3.value;
        if (int.TryParse(textFieldValue, out _vegetableAmount))
        {
            _vegetableAmount++;
            string updatedText = _vegetableAmount.ToString();
            
            _textField3.value = updatedText;
        }
        else
        {
            Debug.LogWarning("Invalid integer value in text field.");
        }
    }
    
    private void OnDecrease1clicked(ClickEvent evt)
    {
        string textFieldValue = _textField1.value;
        if (int.TryParse(textFieldValue, out _flourAmount))
        {
            _flourAmount--;
            if (_flourAmount <= 0)
            {
                _flourAmount = 0;
            }
            string updatedText = _flourAmount.ToString();
            
            _textField1.value = updatedText;
        }
        else
        {
            Debug.LogWarning("Invalid integer value in text field.");
        }
    }
    private void OnDecrease2clicked(ClickEvent evt)
    {
        string textFieldValue = _textField2.value;
        if (int.TryParse(textFieldValue, out _meatAmount))
        {
            _meatAmount--;
            if (_meatAmount <= 0)
            {
                _meatAmount = 0;
            }
            string updatedText = _meatAmount.ToString();
            
            _textField2.value = updatedText;
        }
        else
        {
            Debug.LogWarning("Invalid integer value in text field.");
        }
    }
    private void OnDecrease3clicked(ClickEvent evt)
    {
        string textFieldValue = _textField3.value;
        if (int.TryParse(textFieldValue, out _vegetableAmount))
        {
            _vegetableAmount--;
            if (_vegetableAmount <= 0)
            {
                _vegetableAmount = 0;
            }
            string updatedText = _vegetableAmount.ToString();
            
            _textField3.value = updatedText;
        }
        else
        {
            Debug.LogWarning("Invalid integer value in text field.");
        }
    }


    #endregion

    #region textField funtion

    void OnValue1Changed(ChangeEvent<string> evt)
    {
        string text = evt.newValue;
        
        if (text.Length <= 0)
        {
            _textField1.value = 0.ToString();
            return;
        }
        
        if (int.TryParse(text, out _flourAmount))
        {
            string updatedText = _flourAmount.ToString("D");
            _textField1.value = updatedText;
            Debug.Log("Integer entered: " + _flourAmount);
        }
        else
        {
            _textField1.value = text.Substring(0, text.Length - 1); // Remove last character
        }
    }
    void OnValue2Changed(ChangeEvent<string> evt)
    {
        string text = evt.newValue;
        
        if (text.Length <= 0)
        {
            _textField2.value = 0.ToString();
            return;
        }
        
        if (int.TryParse(text, out _meatAmount))
        {
            string updatedText = _meatAmount.ToString("D");
            _textField2.value = updatedText;
            Debug.Log("Integer entered: " + _meatAmount);
        }
        else
        {
            _textField2.value = text.Substring(0, text.Length - 1); // Remove last character
        }
    }
    void OnValue3Changed(ChangeEvent<string> evt)
    {
        string text = evt.newValue;
        
        if (text.Length <= 0)
        {
            _textField3.value = 0.ToString();
            return;
        }
        
        if (int.TryParse(text, out _vegetableAmount))
        {
            string updatedText = _vegetableAmount.ToString("D");
            _textField3.value = updatedText;
            Debug.Log("Integer entered: " + _vegetableAmount);
        }
        else
        {
            _textField3.value = text.Substring(0, text.Length - 1); // Remove last character
        }
    }

    #endregion

    private void SetCurrentIngredients()
    {
        _flourLabel.text = "Current: " + flourInt;
        _meatLabel.text = "Current: " + meatInt;
        _vegetableLabel.text = "Current: " + vegetableInt;
    }

    private void AddCurrentIngredients()
    {
        flourInt += _flourAmount;
        meatInt += _meatAmount;
        vegetableInt += _vegetableAmount;
    }
    

    public float _elapsedTime;
    private void Update()
    {
        _elapsedTime += Time.deltaTime;
        int minutes = Mathf.FloorToInt(_elapsedTime / 60f);
        int seconds = Mathf.FloorToInt(_elapsedTime % 60f);
        _timeLabel.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        
        _moneyLabel.text = "Coins: " + moneyAmount.ToString();

        HealthBar.style.height = new StyleLength(new Length(GlobalManager.Instance.currentHealth, LengthUnit.Percent));
        //Debug.Log("Current Health: " + GlobalManager.Instance.currentHealth);
        
        SetCurrentIngredients();
        
        total = (_flourAmount*flourCost) + (_meatAmount*meatCost) + (_vegetableAmount*vegetableCost);
        Cost.text = "Cost : " + total.ToString();
    }

    public void SetMenu1()
    {
        //Table1
        if (GlobalManager.Instance.Table1CurrentMenu == 1)
        {
            _customerOrder.style.backgroundImage = GlobalManager.Instance.Order[0];
        }
        else if (GlobalManager.Instance.Table1CurrentMenu == 2)
        {
            _customerOrder.style.backgroundImage = GlobalManager.Instance.Order[1];
        }
        else if (GlobalManager.Instance.Table1CurrentMenu == 3)
        {
            _customerOrder.style.backgroundImage = GlobalManager.Instance.Order[2];
        }
        else if (GlobalManager.Instance.Table1CurrentMenu == 4)
        {
            _customerOrder.style.backgroundImage = GlobalManager.Instance.Order[3];
        }
        else if (GlobalManager.Instance.Table1CurrentMenu == 5)
        {
            _customerOrder.style.backgroundImage = GlobalManager.Instance.Order[4];
        }
        else if (GlobalManager.Instance.Table1CurrentMenu == 6)
        {
            _customerOrder.style.backgroundImage = GlobalManager.Instance.Order[5];
        }
        else if (GlobalManager.Instance.Table1CurrentMenu == 7)
        {
            _customerOrder.style.backgroundImage = GlobalManager.Instance.Order[6];
        }
        else if (GlobalManager.Instance.Table1CurrentMenu == 8)
        {
            _customerOrder.style.backgroundImage = GlobalManager.Instance.Order[7];
        }
    }
    
    public void SetMenu2()
    {
        //Table2
        if (GlobalManager.Instance.Table2CurrentMenu == 1)
        {
            _customerOrder2.style.backgroundImage = GlobalManager.Instance.Order[0];
        }
        else if (GlobalManager.Instance.Table2CurrentMenu == 2)
        {
            _customerOrder2.style.backgroundImage = GlobalManager.Instance.Order[1];
        }
        else if (GlobalManager.Instance.Table2CurrentMenu == 3)
        {
            _customerOrder2.style.backgroundImage = GlobalManager.Instance.Order[2];
        }
        else if (GlobalManager.Instance.Table2CurrentMenu == 4)
        {
            _customerOrder2.style.backgroundImage = GlobalManager.Instance.Order[3];
        }
        else if (GlobalManager.Instance.Table2CurrentMenu == 5)
        {
            _customerOrder2.style.backgroundImage = GlobalManager.Instance.Order[4];
        }
        else if (GlobalManager.Instance.Table2CurrentMenu == 6)
        {
            _customerOrder2.style.backgroundImage = GlobalManager.Instance.Order[5];
        }
        else if (GlobalManager.Instance.Table2CurrentMenu == 7)
        {
            _customerOrder2.style.backgroundImage = GlobalManager.Instance.Order[6];
        }
        else if (GlobalManager.Instance.Table2CurrentMenu == 8)
        {
            _customerOrder2.style.backgroundImage = GlobalManager.Instance.Order[7];
        }
    }
    
    public void SetMenu3()
    {
        //table3
        if (GlobalManager.Instance.Table3CurrentMenu == 1)
        {
            _customerOrder3.style.backgroundImage = GlobalManager.Instance.Order[0];
        }
        else if (GlobalManager.Instance.Table3CurrentMenu == 2)
        {
            _customerOrder3.style.backgroundImage = GlobalManager.Instance.Order[1];
        }
        else if (GlobalManager.Instance.Table3CurrentMenu == 3)
        {
            _customerOrder3.style.backgroundImage = GlobalManager.Instance.Order[2];
        }
        else if (GlobalManager.Instance.Table3CurrentMenu == 4)
        {
            _customerOrder3.style.backgroundImage = GlobalManager.Instance.Order[3];
        }
        else if (GlobalManager.Instance.Table3CurrentMenu == 5)
        {
            _customerOrder3.style.backgroundImage = GlobalManager.Instance.Order[4];
        }
        else if (GlobalManager.Instance.Table3CurrentMenu == 6)
        {
            _customerOrder3.style.backgroundImage = GlobalManager.Instance.Order[5];
        }
        else if (GlobalManager.Instance.Table3CurrentMenu == 7)
        {
            _customerOrder3.style.backgroundImage = GlobalManager.Instance.Order[6];
        }
        else if (GlobalManager.Instance.Table3CurrentMenu == 8)
        {
            _customerOrder3.style.backgroundImage = GlobalManager.Instance.Order[7];
        }
    }
    
    public void SetMenu4()
    {
        //table4
        if (GlobalManager.Instance.Table4CurrentMenu == 1)
        {
            _customerOrder4.style.backgroundImage = GlobalManager.Instance.Order[0];
        }
        else if (GlobalManager.Instance.Table4CurrentMenu == 2)
        {
            _customerOrder4.style.backgroundImage = GlobalManager.Instance.Order[1];
        }
        else if (GlobalManager.Instance.Table4CurrentMenu == 3)
        {
            _customerOrder4.style.backgroundImage = GlobalManager.Instance.Order[2];
        }
        else if (GlobalManager.Instance.Table4CurrentMenu == 4)
        {
            _customerOrder4.style.backgroundImage = GlobalManager.Instance.Order[3];
        }
        else if (GlobalManager.Instance.Table4CurrentMenu == 5)
        {
            _customerOrder4.style.backgroundImage = GlobalManager.Instance.Order[4];
        }
        else if (GlobalManager.Instance.Table4CurrentMenu == 6)
        {
            _customerOrder4.style.backgroundImage = GlobalManager.Instance.Order[5];
        }
        else if (GlobalManager.Instance.Table4CurrentMenu == 7)
        {
            _customerOrder4.style.backgroundImage = GlobalManager.Instance.Order[6];
        }
        else if (GlobalManager.Instance.Table4CurrentMenu == 8)
        {
            _customerOrder4.style.backgroundImage = GlobalManager.Instance.Order[7];
        }
    }
    
    public void SetMenu5()
    {
        //table5
        if (GlobalManager.Instance.Table5CurrentMenu == 1)
        {
            _customerOrder5.style.backgroundImage = GlobalManager.Instance.Order[0];
        }
        else if (GlobalManager.Instance.Table5CurrentMenu == 2)
        {
            _customerOrder5.style.backgroundImage = GlobalManager.Instance.Order[1];
        }
        else if (GlobalManager.Instance.Table5CurrentMenu == 3)
        {
            _customerOrder5.style.backgroundImage = GlobalManager.Instance.Order[2];
        }
        else if (GlobalManager.Instance.Table5CurrentMenu == 4)
        {
            _customerOrder5.style.backgroundImage = GlobalManager.Instance.Order[3];
        }
        else if (GlobalManager.Instance.Table5CurrentMenu == 5)
        {
            _customerOrder5.style.backgroundImage = GlobalManager.Instance.Order[4];
        }
        else if (GlobalManager.Instance.Table5CurrentMenu == 6)
        {
            _customerOrder5.style.backgroundImage = GlobalManager.Instance.Order[5];
        }
        else if (GlobalManager.Instance.Table5CurrentMenu == 7)
        {
            _customerOrder5.style.backgroundImage = GlobalManager.Instance.Order[6];
        }
        else if (GlobalManager.Instance.Table5CurrentMenu == 8)
        {
            _customerOrder5.style.backgroundImage = GlobalManager.Instance.Order[7];
        }
    }

    public void ResetMenu(int Index)
    {
        if (Index == 1)
        {
            _customerOrder.style.backgroundImage = GlobalManager.Instance.Order[8];
        }
        if (Index == 2)
        {
            _customerOrder2.style.backgroundImage = GlobalManager.Instance.Order[8];
        }
        if (Index == 3)
        {
            _customerOrder3.style.backgroundImage = GlobalManager.Instance.Order[8];
        }
        if (Index == 4)
        {
            _customerOrder4.style.backgroundImage = GlobalManager.Instance.Order[8];
        }
        if (Index == 5)
        {
            _customerOrder5.style.backgroundImage = GlobalManager.Instance.Order[8];
        }
    }

    public void ResetRecipe(int Index)
    {
        if (Index == 0)
        {
            Debug.LogWarning("Yeet");
        }
        if (Index == 1)
        {
            Recipe[0].style.display = DisplayStyle.None;
            Debug.LogWarning("Reset Recipe 1");
        }
        if (Index == 2)
        {
            Recipe[1].style.display = DisplayStyle.None;
            Debug.LogWarning("Reset Recipe 2");
        }
        if (Index == 3)
        {
            Recipe[2].style.display = DisplayStyle.None;
            Debug.LogWarning("Reset Recipe 3");
        }
        if (Index == 4)
        {
            Recipe[3].style.display = DisplayStyle.None;
            Debug.LogWarning("Reset Recipe 4");
        }
        if (Index == 5)
        {
            Recipe[4].style.display = DisplayStyle.None;
            Debug.LogWarning("Reset Recipe 5");
        }
        if (Index == 6)
        {
            Recipe[5].style.display = DisplayStyle.None;
            Debug.LogWarning("Reset Recipe 6");
        }
        if (Index == 7)
        {
            Recipe[6].style.display = DisplayStyle.None;
            Debug.LogWarning("Reset Recipe 7");
        }
        if (Index == 8)
        {
            Recipe[0].style.display = DisplayStyle.None;
            Debug.LogWarning("Reset Recipe 8");
        }
    }

    public void CreateRecipe(int Index)
    {
        if (Index == 1)
        {
            Recipe[0].style.display = DisplayStyle.Flex;
        }
        if (Index == 2)
        {
            Recipe[1].style.display = DisplayStyle.Flex;
        }
        if (Index == 3)
        {
            Recipe[2].style.display = DisplayStyle.Flex;
        }
        if (Index == 4)
        {
            Recipe[3].style.display = DisplayStyle.Flex;
        }
        if (Index == 5)
        {
            Recipe[4].style.display = DisplayStyle.Flex;
        }
        if (Index == 6)
        {
            Recipe[5].style.display = DisplayStyle.Flex;
        }
        if (Index == 7)
        {
            Recipe[6].style.display = DisplayStyle.Flex;
        }
        if (Index == 8)
        {
            Recipe[7].style.display = DisplayStyle.Flex;
        }
    }
}
