using UnityEngine;
using UnityEngine.UI;

public class InvertCursorMovement : MonoBehaviour
{
    public Image targetImage; // Assign the Image component you want to invert in the Inspector
    public bool invertX = true;
    public bool invertY = false;
    
    void Update()
    {
        Vector2 mousePosition = Input.mousePosition;

        // Invert mouse position if desired
        if (invertX) mousePosition.x = Screen.width - mousePosition.x;
        if (invertY) mousePosition.y = Screen.height - mousePosition.y;

        // Apply inverted mouse position to Image's rect transform
        targetImage.rectTransform.anchoredPosition = mousePosition;
        
    }

}

